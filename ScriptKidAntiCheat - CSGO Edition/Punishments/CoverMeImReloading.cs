﻿using ScriptKidAntiCheat.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace ScriptKidAntiCheat.Punishments
{
    // Player after one or few bullets shot reloads the gun
    class CoverMeImReloading : Punishment
    {
        public bool Active;

        public int LastAmmoCount = 0;

        public Weapons LastActiveWeapon = 0;

        public CoverMeImReloading() : base(0, false, 50)
        {
            LastAmmoCount = Program.GameData.Player.AmmoCount;
            LastActiveWeapon = (Weapons)Program.GameData.Player.ActiveWeapon;
            Reset(); // to set Active to randon value
            Program.GameData.MatchInfo.OnMatchNewRound += OnNewRound;
        }

        private void OnNewRound(object sender, EventArgs e)
        {
            Reset();
        }

        public override void Reset()
        {
            // 20% chance to activate in current round
            int rnd = new Random().Next(4);
            Active = rnd == 0;
        }

        public override void Tick(object source, ElapsedEventArgs e)
        {

            try
            {
                int CurrentAmmoCount = Program.GameData.Player.AmmoCount;
                Weapons CurrentActiveWeapon = (Weapons)Program.GameData.Player.ActiveWeapon;

                if (Active && CurrentActiveWeapon == LastActiveWeapon && CurrentAmmoCount < LastAmmoCount)
                {
                    ActivatePunishment();
                }

                LastAmmoCount = CurrentAmmoCount;
                LastActiveWeapon = CurrentActiveWeapon;
            }
            catch (Exception ex)
            {
                // yeet
            }
        }

        public void ActivatePunishment()
        {
            if (base.CanActivate() == false) return;

            Task.Run(() =>
            {
                Program.GameConsole.SendCommand("unbind mouse1; -attack; +reload;");
                Thread.Sleep(400);
                Program.GameConsole.SendCommand("bind mouse1 +attack; -reload;");
            });
            base.AfterActivate();
        }
    }
}
